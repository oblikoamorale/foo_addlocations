#include "stdafx.h"

void RunAddLocations(); //add_locations.cpp

class mainmenu_commands_add_locations : public mainmenu_commands {
public:
	enum {
		cmd_addlocations = 0,
		cmd_total
	};
	t_uint32 get_command_count() {
		return cmd_total;
	}
	GUID get_command(t_uint32 p_index) {

		static const GUID guid_addlocations = { 0x3e178a96, 0xd237, 0x4266,{ 0x9f, 0xca, 0xc4, 0xd3, 0x3e, 0xa8, 0xe4, 0xc7 } };

		switch(p_index) {
			case cmd_addlocations: return guid_addlocations;
			default: uBugCheck(); // should never happen unless somebody called us with invalid parameters - bail
		}
	}
	void get_name(t_uint32 p_index,pfc::string_base & p_out) {
		switch(p_index) {
			case cmd_addlocations: p_out = "Add locations..."; break;
			default: uBugCheck(); // should never happen unless somebody called us with invalid parameters - bail
		}
	}
	bool get_description(t_uint32 p_index,pfc::string_base & p_out) {
		switch(p_index) {
			case cmd_addlocations: p_out = "Adds the specified URLs to the current playlist."; return true;
			default: uBugCheck(); // should never happen unless somebody called us with invalid parameters - bail
		}
	}
	GUID get_parent() {
		return mainmenu_groups::file_add;
	}
	void execute(t_uint32 p_index,service_ptr_t<service_base> p_callback) {
		switch(p_index) {
			case cmd_addlocations:
				RunAddLocations();
				break;
			default:
				uBugCheck(); // should never happen unless somebody called us with invalid parameters - bail
		}
	}
};

static mainmenu_commands_factory_t<mainmenu_commands_add_locations> g_mainmenu_commands_sample_factory;
