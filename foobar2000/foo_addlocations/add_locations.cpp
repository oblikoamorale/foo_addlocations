#include "stdafx.h"
#include "resource.h"

static HWND add_locations_dlg = NULL;

class process_locations_notify_add	 : public process_locations_notify {
public:
	void on_completion(const pfc::list_base_const_t<metadb_handle_ptr> & p_items) { 
		static_api_ptr_t<playlist_manager> plm;

		if (plm->get_active_playlist() != pfc::infinite_size)
			plm->activeplaylist_add_items(p_items, bit_array_true());
		else
			popup_message::g_complain("Cannot add locations: no active playlist.");
	};

	void on_aborted() { };
};

class CAddLocations : public CDialogImpl<CAddLocations> {
public:
	enum { IDD = IDD_ADD_LOCATIONS };

	BEGIN_MSG_MAP(CPlaybackStateDemo)
		MSG_WM_INITDIALOG(OnInitDialog)
		MSG_WM_DESTROY(OnDestroy)
		COMMAND_HANDLER_EX(IDOK, BN_CLICKED, OnOK)
		COMMAND_HANDLER_EX(IDCANCEL, BN_CLICKED, OnCancel)
	END_MSG_MAP()
	
private:
	void OnCancel(UINT, int, CWindow);
	void OnOK(UINT, int, CWindow);

	void OnDestroy();
	BOOL OnInitDialog(CWindow, LPARAM);
};

void CAddLocations::OnDestroy() {
	add_locations_dlg = NULL;
}

void CAddLocations::OnCancel(UINT, int, CWindow) {
	DestroyWindow();
}

void CAddLocations::OnOK(UINT, int, CWindow) {
	static_api_ptr_t<playlist_manager> plm;
	pfc::string8 locations;
	pfc::string_list_impl locations_list;

	uGetDlgItemText(*this, IDC_LOCATIONS, locations);
	pfc::splitStringByLines(locations_list, locations);

	if (plm->get_active_playlist() != pfc::infinite_size && locations_list.get_count()) {
		plm->activeplaylist_undo_backup();

		static_api_ptr_t<playlist_incoming_item_filter_v2>()->process_locations_async(
			locations_list,
			NULL,
			NULL,
			NULL,
			core_api::get_main_window(),
			new service_impl_t<process_locations_notify_add>()
		);
	}

	DestroyWindow();
}

BOOL CAddLocations::OnInitDialog(CWindow, LPARAM) {
	::ShowWindowCentered(*this, GetParent());
	
	GotoDlgCtrl(uGetDlgItem(IDC_LOCATIONS));

	add_locations_dlg = *this;

	return FALSE;
}

void RunAddLocations() {
	try {
		if (add_locations_dlg != NULL)
			SetActiveWindow(add_locations_dlg);
		else
			new CWindowAutoLifetime<ImplementModelessTracking<CAddLocations> >(core_api::get_main_window());
	}
	catch (std::exception const & e) {
		popup_message::g_complain("Dialog creation failure", e);
	}
}
